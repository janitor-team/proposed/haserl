haserl (0.9.36-1) unstable; urgency=medium

  * [339edb7] d/watch: Tighten regex.
    It was picking up "devel/haserl-0.9.35" as a version.
  * [5861a8b] New upstream version 0.9.36:
    - Fix sf.net issue #5 - its possible to issue a PUT request
      without a CONTENT-TYPE. Assume an octet-stream in that case.
    - (BREAKING CHANGE) Change the Prefix for variables to be the REQUEST_METHOD
      (PUT/DELETE/GET/POST)
    * Mitigations vs running haserl to get access to files not
      available to the user.
  * [63f9692] Bump dh compat level to 12
  * [e3768b1] Bump Standards-Version to 4.5.1
  * [44e30bd] Update Vcs-* URIs
  * [253322f] Drop get-orig-source rule
    uscan can handle it these days
  * [419769e] Drop redundant autotools-dev build-dep

 -- Chow Loong Jin <hyperair@debian.org>  Tue, 09 Mar 2021 01:11:25 +0800

haserl (0.9.35-2) unstable; urgency=medium

  * [1e361db] Update pt translation.
    Thanks to Adriano Rafael Gomes <adrianorg@arg.eti.br> (Closes: #827331)
  * [d892e0a] No-change bump of Standards-Version to 3.9.8

 -- Chow Loong Jin <hyperair@debian.org>  Sat, 18 Jun 2016 22:00:32 +0800

haserl (0.9.35-1) experimental; urgency=medium

  * [14c6485] Imported Upstream version 0.9.35

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 15 Mar 2015 10:25:25 +0800

haserl (0.9.34-1) experimental; urgency=medium

  * [8f33166] Imported Upstream version 0.9.34
  * [88cf705] Bump Standards-Version to 3.9.6

 -- Chow Loong Jin <hyperair@debian.org>  Thu, 19 Feb 2015 04:36:21 +0800

haserl (0.9.33-1) unstable; urgency=medium

  * [16945f6] Imported Upstream version 0.9.33
  * [ac08f26] No-change bump of Standards-Version to 3.9.5

 -- Chow Loong Jin <hyperair@debian.org>  Wed, 06 Aug 2014 12:46:56 +0800

haserl (0.9.32-1) unstable; urgency=low

  * [af58aed] Imported Upstream version 0.9.32:
    - Fix regression causing Lua always to be linked, never used

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 22 Sep 2013 02:11:09 +0800

haserl (0.9.31-1) unstable; urgency=low

  * [ad3ad8e] Imported Upstream version 0.9.31:
    - Modernize configure.ac
    - more Lua 5.2 fixes
  * [60ece78] Update haserl to use lua5.2

 -- Chow Loong Jin <hyperair@debian.org>  Thu, 19 Sep 2013 00:48:08 +0800

haserl (0.9.30-1) unstable; urgency=low

  * [284920f] Imported Upstream version 0.9.30
    - Fixes segfault when first argument is '' or "" (null-quoted string),
      discovered by the Mayhem Team of CMU.
    - Natanael Copa supplied a patch to rename the deprecated string.gfind
      in haserl_lualib.lua. Lua 5.2 is now supported.
  * [85686f1] Bump dh compat to 9 for buildflags

 -- Chow Loong Jin <hyperair@debian.org>  Mon, 01 Jul 2013 10:37:37 +0800

haserl (0.9.29-4) experimental; urgency=low

  * [97ed12e] Add Japanese translation for debconf template.
    Thanks to victory <victory.deb@gmail.com> (Closes: #692973)
  * [a346a2c] No-change bump of Standards-Version to 3.9.4

 -- Chow Loong Jin <hyperair@debian.org>  Mon, 12 Nov 2012 18:58:10 +0800

haserl (0.9.29-3) unstable; urgency=low

  * [ce39c59] Update Slovak (sk) debconf translation.
    Thanks to Slavko (Closes: #651730)
  * [b75ced0] Add Swedish (sv) debconf translation.
    Thanks to Martin Bagge / brother (Closes: #640033)
  * [57afc1a] Update maintainer email

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 08 Jan 2012 01:16:10 +0800

haserl (0.9.29-2) unstable; urgency=low

  [ Christian Perrier ]
  * [81b0b68] Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project. Closes: #636101
    - Slovak (Slavko).  Closes: #636573
    - Danish (Joe Hansen).  Closes: #636950
    - Russian (Yuri Kozlov).  Closes: #637010
    - German (Chris Leick).  Closes: #637290
    - French (Julien Patriarca).  Closes: #637417
    - Czech (Michal Simunek).  Closes: #637800
    - Portuguese (Rui Branco).  Closes: #638179
    - Spanish; (Francisco Javier Cuadrado).  Closes: #638330
    - Dutch; (Jeroen Schot).  Closes: #638700

  [ Chow Loong Jin ]
  * [648dd86] No-change Standards-Version bump (3.9.1 → 3.9.2)
  * [43bf76c] Add missing License: field in debian/copyright

 -- Chow Loong Jin <hyperair@ubuntu.com>  Tue, 27 Dec 2011 01:11:56 +0800

haserl (0.9.29-1) unstable; urgency=low

  * Initial release (Closes: #611445)

 -- Chow Loong Jin <hyperair@ubuntu.com>  Wed, 11 May 2011 01:53:02 +0800
