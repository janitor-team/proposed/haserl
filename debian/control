Source: haserl
Section: interpreters
Priority: optional
Maintainer: Chow Loong Jin <hyperair@debian.org>
Build-Depends: debhelper-compat (= 12),
               po-debconf,
               liblua5.2-dev,
               pkg-config
Standards-Version: 4.5.1
Homepage: http://haserl.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/haserl.git
Vcs-Browser: https://salsa.debian.org/debian/haserl

Package: haserl
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: lua5.2
Description: CGI scripting program for embedded environments
 Haserl is a CGI wrapper that allows PHP-style programming in Lua or a
 POSIX-compliant shell. It is very small, so it can be used in embedded
 environments, or where something like PHP is too big.
 .
 It combines three features into a small CGI engine:
  * It parses POST and GET requests, placing form-elements into the
    environment as name=value pairs for the CGI script to use. This is
    somewhat similar to the uncgi wrapper.
  * It opens a shell, and translates all text into printable statements.
    All text within <% ... %> constructs is passed verbatim to the shell.
    This is somewhat similar to how PHP scripts are parsed.
  * It can be set up to drop its permissions to the owner of the script,
    giving it some of the security features of suexec or cgiwrapper.
